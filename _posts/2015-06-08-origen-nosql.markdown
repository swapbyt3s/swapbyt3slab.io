---
layout: post
title:  "Origen de la palabra NoSQL"
date:   2015-06-08
categories: Base de Datos
---

Es curioso como se desencadena toda una revolución por un simple hashtag. Aquí les dejo una breve historia de su verdadero origen:

Johan Oskarsson organizó un evento en Junio de 2009 en San Francisco, la intención era discutir las nuevas tecnologías en el mundo IT sobre el almacenamiento y procesamiento de datos. La principal razón del evento fueron los nuevos productos como BigTable y Dynamo. Para el evento era necesario encontrar una palabra clave para ser usada en un hashtag de Twitter, término siendo **"[NoSQL](http://es.wikipedia.org/wiki/NoSQL)"** y fue sugerido por Eric Evans de RackSpace. El término fue planeado para ser utilizado sólo para ésta reunión y no tenía un significado profundo. Pero resultó que se extendió por la red de forma viral y se convirtió en el nombre de facto de una tendencia de las bases de datos no relacionales y distribuidas.

El término "NoSQL" tiene un origen absolutamente natural y éste no está avalado por una institución científica. Se considera que el término esta lejos de su completa definición, hay autores como Pramod J. Sadalage y Martin Fowler que trataron de agrupar y organizar todo el conocimiento sobre el mundo NoSQL en el libro "[NoSQL destilada](http://www.amazon.com/NoSQL-Distilled-Emerging-Polyglot-Persistence/dp/0321826620)".

**Fuentes:**

* [Introduction to NoSQL by Martin Fowler](https://www.youtube.com/watch?v=qI_g07C_Q5I)
* [World of the NoSQL databases](http://leopard.in.ua/2013/11/08/nosql-world/)
