---
layout: page
title: Libros leídos
permalink: /books/
---

Aún no he escrito ningún libro, pero quien sabe... Esta sección es solo para compartir los libros que he leído y me han encantado mucho:

## En general:

- Armada
- El Marciano
- El Naufrago
- Ready Player One
- Saga de La vieja guardia
- Steve Jobs

## Sobre informática:

- Big data. La revolución de los datos masivos
- El libro negro del programador
- El método Lean Startup
- Gestión práctica de proyectos con Scrum
- Los principales errores de los emprendedores
- The Art of Capacity Planning: Scaling Web Resources
